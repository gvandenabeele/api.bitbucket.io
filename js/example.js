//multiple methods to get data asynchronous from a API.
const asyncWithJquery = () => {
    $.get('https://randomuser.me/api/?results=10')
        .done(asycnSuccessCallbackHandler)
        .fail(asycnFailCallbackHandler);
}
//jquery call is successfull
const asycnSuccessCallbackHandler = (data) => {
    console.log('my results', data)
}

//jquery call is failed
const asycnFailCallbackHandler = (err) => {
    console.error('Failed', err);
}

//es6
const asyncWithFetch = () => {
    console.log('getting startels');
    //url (required), options (optional)
    var request = new Request('https://randomuser.me/api/?results=10', {
        method: 'GET',
        headers: new Headers({
            'Content-Type': 'application/json'
        })
    });

    fetch(request)
        .then((resp) => {
            if(!resp.ok){
                throw Error(resp.statusText);
            }
            return resp.json();
        }).then(function (data) { /* handle response */
            console.log(data);
        }).catch((error) => {
            console.error(error);
        });

}

//default 'old school'
const asyncWithXmlHTTPRequest = () => {
    // using XMLHttpRequest
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "https://randomusers.me/api/?results=10", true);
    xhr.onreadystatechange = function (oEvent) {  
        //request is done https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/readyState
        if (xhr.readyState === 4) {  
            //request went well
            if (xhr.status === 200) {  
              console.log(xhr.responseText)  
            } else {  
               console.log("Error", xhr.statusText);  
            }  
        }  
    };   
    xhr.send();
}



//wait until DOM is loaded
window.addEventListener("load", init);

/**
 * Initialize the application (after DOM ready)
 */
function init() {
    // asyncWithXmlHTTPRequest();
    asyncWithJquery();
    // asyncWithFetch();  
}