var kelvin = 273.15;
var city = '';
var lat = 0;
var lon = 0;
//jquery call
const asyncWithJquery = () => {
    $.get('http://api.openweathermap.org/data/2.5/weather?APPID=3c6ad9980b87cb48863ad4b39e6cb409&q='+city)
        .done(asycnSuccessCallbackHandler)
        .fail(asycnFailCallbackHandler);
}
//jquery call is successfull
const asycnSuccessCallbackHandler = (data) => {
    //console.log(data)
    console.log(data);
        //Create main div
        var newDiv = document.createElement("div");
        //Create Title
        var newTitle = document.createElement("h1");
        //Create icon
        var newImage = document.createElement("img");
        newImage.src = "http://openweathermap.org/img/w/"+data.weather[0].icon+".png";
        var newTitleText = document.createTextNode(data.name); 
        var newContentText = document.createTextNode(data.weather[0].description+' en '+String(Math.round(parseInt(data.main.temp)-kelvin), -1) + ' graden'); 
        newTitle.appendChild(newTitleText);
        newDiv.appendChild(newTitle);
        newDiv.appendChild(newImage);
        newDiv.appendChild(newContentText); //add the text node to the newly created div. 
      
        // add the newly created element and its content into the DOM 
        document.getElementById('users').appendChild(newDiv);
        lat = data.coord.lat;
        lon = data.coord.lon;
        initMap();
}

//jquery call is failed
const asycnFailCallbackHandler = (err) => {
    console.error('Failed', err);
}

function getCityData(c){
    city = c;
    document.getElementById('users').innerHTML = '';
    asyncWithJquery();
    
}


